﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidBody
{
    GameObject box;
    Vector3 position;
    Vector3 speed;

    //box 설정 및 return 가능
    public GameObject Box
    {
        get { return box; }
        set { box = value; }
    }

    //position 값 설정 및 return 가능
    public Vector3 Position
    {
        get { return box.transform.position; }
        set { box.transform.position = value; }
    }

    //scale 값 설정 및 return 가능
    public Vector3 Scale
    {
        get { return box.transform.localScale; }
        set { box.transform.localScale = value; }
    }

    //speed 값 설정 및 return 가능
    public Vector3 Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    //Rotation 값 설정 및 return 가능
    public UnityEngine.Quaternion Rotation
    {
        get { return box.transform.rotation; }
        set { box.transform.rotation = value; }
    }

    //color 값 설정 및 return 가능
    public Color Color
    {
        get { return box.GetComponent<MeshRenderer>().material.color; }
        set { box.GetComponent<MeshRenderer>().material.color = value; }
    }

    public AABBCollider boxCollider = new AABBCollider();
    public OBBCollider boxOBBCollider = new OBBCollider();

    //Position Update 함수 Speed 값 더해줌
    public void UpdatePosition()
    {
        position.x = box.transform.position.x + speed.x * Time.deltaTime;
        position.y = box.transform.position.y + speed.y * Time.deltaTime;
        position.z = box.transform.position.z + speed.z * Time.deltaTime;
        box.transform.position = position;

        boxOBBCollider.SetCollider(box.transform.position, box.transform.rotation, box.transform.localScale);
        CheckMapOutSide();
    }

    //Map 밖으로 나갔는지 판단하고 나갔다면 Speed 반대로 설정
    public void CheckMapOutSide()
    {
        //Vector3 maxPos = Camera.main.WorldToViewportPoint(box.transform.position + box.transform.localScale / 2);
        //Vector3 minPos = Camera.main.WorldToViewportPoint(box.transform.position - box.transform.localScale / 2);

        if (box.transform.position.x < -4f || box.transform.position.x > 4f)
        {
            speed.x = -speed.x;
        }

        if (box.transform.position.y < -4f || box.transform.position.y > 4f)
        {
            speed.y = -speed.y;
        }

        if (box.transform.position.z < -4f || box.transform.position.z > 4f)
        {
            speed.z = -speed.z;
        }
    }
}
