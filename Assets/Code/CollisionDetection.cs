﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    //AABB collision Detection 실행 
    //https://www.youtube.com/watch?v=rjirHFVQAkY 참고
    public static bool AABBCollisionDetection(AABBCollider A, AABBCollider B)
    {
        if (A.MinX + A.W >= B.MinX &&
            B.MinX + B.W >= A.MinX &&
            A.MinY + A.H >= B.MinY &&
            B.MinY + B.H >= A.MinY &&
            A.MinZ + A.D >= B.MinZ &&
            B.MinZ + B.D >= A.MinZ)
        {
            return true;
        }
        return false;

    }
    static float DotProduct(float[] a, float[] b)
    {
        return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
    }
    public static bool OBBCollisionDetection(OBBCollider A, OBBCollider B)
    {
        float[] D = new float[3];
        D[0] = B.Center[0] - A.Center[0];
        D[1] = B.Center[1] - A.Center[1];
        D[2] = B.Center[2] - A.Center[2];

        float[,] C = new float[3, 3];
        float[,] absC = new float[3, 3];
        float[] AD = new float[3];
        float R0, R1, R;
        float R01;

        float[] Aaxis0 = new float[3] { A.Axis[0, 0], A.Axis[0, 1], A.Axis[0, 2] };
        float[] Aaxis1 = new float[3] { A.Axis[1, 0], A.Axis[1, 1], A.Axis[1, 2] };
        float[] Aaxis2 = new float[3] { A.Axis[2, 0], A.Axis[2, 1], A.Axis[2, 2] };

        float[] Baxis0 = new float[3] { B.Axis[0, 0], B.Axis[0, 1], B.Axis[0, 2] };
        float[] Baxis1 = new float[3] { B.Axis[1, 0], B.Axis[1, 1], B.Axis[1, 2] };
        float[] Baxis2 = new float[3] { B.Axis[2, 0], B.Axis[2, 1], B.Axis[2, 2] };

        //A0
        C[0, 0] = DotProduct(Aaxis0, Baxis0);
        C[0, 1] = DotProduct(Aaxis0, Baxis1);
        C[0, 2] = DotProduct(Aaxis0, Baxis2);

        AD[0] = DotProduct(Aaxis0, D);

        absC[0, 0] = Math.Abs(C[0, 0]);
        absC[0, 1] = Math.Abs(C[0, 1]);
        absC[0, 2] = Math.Abs(C[0, 2]);
        R = Math.Abs(AD[0]);
        R1 = B.Extent[0] * absC[0, 0] + B.Extent[1] * absC[0, 1] + B.Extent[2] * absC[0, 2];
        R01 = A.Extent[0] + R1;

        if (R > R01)
            return false;

        //A1
        C[1, 0] = DotProduct(Aaxis1, Baxis0);
        C[1, 1] = DotProduct(Aaxis1, Baxis1);
        C[1, 2] = DotProduct(Aaxis1, Baxis2);
        
        AD[1] = DotProduct(Aaxis1, D);

        absC[1, 0] = Math.Abs(C[1, 0]);
        absC[1, 1] = Math.Abs(C[1, 1]);
        absC[1, 2] = Math.Abs(C[1, 2]);
        R = Math.Abs(AD[1]);
        R1 = B.Extent[0] * absC[1, 0] + B.Extent[1] * absC[1, 1] + B.Extent[2] * absC[1, 2];
        R01 = A.Extent[1] + R1;

        if (R > R01)
            return false;

        //A2
        C[2, 0] = DotProduct(Aaxis2, Baxis0);
        C[2, 1] = DotProduct(Aaxis2, Baxis1);
        C[2, 2] = DotProduct(Aaxis2, Baxis2);
        
        AD[2] = DotProduct(Aaxis2, D);

        absC[2, 0] = Math.Abs(C[2, 0]);
        absC[2, 1] = Math.Abs(C[2, 1]);
        absC[2, 2] = Math.Abs(C[2, 2]);
        R = Math.Abs(AD[2]);
        R1 = B.Extent[0] * absC[2, 0] + B.Extent[1] * absC[2, 1] + B.Extent[2] * absC[2, 2];
        R01 = A.Extent[2] + R1;

        if (R > R01)
            return false;

        //B0
        R = Math.Abs(DotProduct(Aaxis0, D));
        R0 = A.Extent[0] * absC[0, 0] + A.Extent[1] * absC[1, 0] + A.Extent[2] * absC[2, 0];
        R01 = R0 + B.Extent[0];

        if (R > R01)
            return false;

        //B1
        R = Math.Abs(DotProduct(Aaxis1, D));
        R0 = A.Extent[0] * absC[0, 1] + A.Extent[1] * absC[1, 1] + A.Extent[2] * absC[2, 1];
        R01 = R0 + B.Extent[1];

        if (R > R01)
            return false;

        //B2
        R = Math.Abs(DotProduct(Aaxis2, D));
        R0 = A.Extent[0] * absC[0, 2] + A.Extent[1] * absC[1, 2] + A.Extent[2] * absC[2, 2];
        R01 = R0 + B.Extent[2];

        if (R > R01)
            return false;

        //A0 X B0
        R = Math.Abs(AD[2] * C[1, 0] - AD[1] * C[2, 0]);
        R0 = A.Extent[1] * absC[2, 0] + A.Extent[2] * absC[1, 0];
        R0 = B.Extent[1] * absC[0, 2] + B.Extent[2] * absC[0, 1];
        R01 = R0 + R1;

        if (R > R01)
            return false;

        //A1 X B0
        R = Math.Abs(AD[2] * C[1, 1] - AD[1] * C[2, 1]);
        R0 = A.Extent[1] * absC[2, 1] + A.Extent[2] * absC[1, 1];
        R0 = B.Extent[0] * absC[0, 2] + B.Extent[2] * absC[0, 0];
        R01 = R0 + R1;

        if (R > R01)
            return false;

        //A2 X B0
        R = Math.Abs(AD[2] * C[1, 2] - AD[1] * C[2, 2]);
        R0 = A.Extent[1] * absC[2, 2] + A.Extent[2] * absC[1, 2];
        R0 = B.Extent[0] * absC[0, 1] + B.Extent[1] * absC[0, 0];
        R01 = R0 + R1;

        if (R > R01)
            return false;

        //A0 X B1
        R = Math.Abs(AD[0] * C[2, 0] - AD[2] * C[0, 0]);
        R0 = A.Extent[0] * absC[2, 0] + A.Extent[2] * absC[0, 0];
        R0 = B.Extent[1] * absC[1, 2] + B.Extent[2] * absC[1, 1];
        R01 = R0 + R1;

        if (R > R01)
            return false;

        //A1 X B1
        R = Math.Abs(AD[0] * C[2, 1] - AD[2] * C[0, 1]);
        R0 = A.Extent[0] * absC[2, 1] + A.Extent[2] * absC[0, 1];
        R0 = B.Extent[0] * absC[1, 2] + B.Extent[2] * absC[1, 0];
        R01 = R0 + R1;

        if (R > R01)
            return false;

        //A2 X B1
        R = Math.Abs(AD[0] * C[2, 2] - AD[2] * C[0, 2]);
        R0 = A.Extent[0] * absC[2, 2] + A.Extent[2] * absC[0, 2];
        R0 = B.Extent[0] * absC[1, 1] + B.Extent[1] * absC[1, 0];
        R01 = R0 + R1;

        if (R > R01)
            return false;

        //A0 X B2
        R = Math.Abs(AD[1] * C[0, 0] - AD[0] * C[1, 0]);
        R0 = A.Extent[0] * absC[1, 0] + A.Extent[1] * absC[0, 0];
        R0 = B.Extent[1] * absC[2, 2] + B.Extent[2] * absC[2, 1];
        R01 = R0 + R1;

        if (R > R01)
            return false;

        //A1 X B2
        R = Math.Abs(AD[1] * C[0, 1] - AD[0] * C[1, 1]);
        R0 = A.Extent[0] * absC[1, 1] + A.Extent[1] * absC[0, 1];
        R0 = B.Extent[0] * absC[2, 2] + B.Extent[2] * absC[2, 0];
        R01 = R0 + R1;

        if (R > R01)
            return false;

        //A2 X B2
        R = Math.Abs(AD[1] * C[0, 2] - AD[0] * C[1, 2]);
        R0 = A.Extent[0] * absC[1, 2] + A.Extent[1] * absC[0, 2];
        R0 = B.Extent[0] * absC[2, 1] + B.Extent[1] * absC[2, 0];
        R01 = R0 + R1;

        if (R > R01)
            return false;

        return true; 
    }
    
}
