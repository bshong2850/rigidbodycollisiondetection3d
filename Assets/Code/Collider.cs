﻿using UnityEngine;
public class AABBCollider
{
    private float minX;
    private float minY;
    private float minZ;
    private float w;
    private float h;
    private float d;

    public float MinX
    {
        get { return minX; }
    }
    public float MinY
    {
        get { return minY; }
    }
    public float MinZ
    {
        get { return minZ; }
    }
    public float W
    {
        get { return w; }
    }
    public float H
    {
        get { return h; }
    }
    public float D
    {
        get { return d; }
    }

    // Start is called before the first frame update
    void Start()
    {
        minX = 0.0f;
        minY = 0.0f;
        minZ = 0.0f;
        w = 0.0f;
        h = 0.0f;
    }

    public void SetCollider(Vector3 position, Vector3 scale)
    {
        minX = position.x - scale.x / 2;
        minY = position.y - scale.y / 2;
        minZ = position.z - scale.z / 2;
        w = scale.x;
        h = scale.y;
        d = scale.z;
    }
}

public class OBBCollider
{
    private float[] center = new float[3];
    private float[,] axis = new float[3, 3];
    private float[] extent = new float[3];
    private float[] translation = new float[3];

    public float[] Center
    {
        get { return center; }
        set { center = value; }
    }
    public float[,] Axis
    {
        get { return axis; }
        set { axis = value; }
    }

    public float[] Extent
    {
        get { return extent; }
        set { extent = value; }
    }    

    // Start is called before the first frame update
    void Start()
    {
        center = new float[3] { 0f, 0f, 0f };
        axis = new float[3, 3] { { 0f, 0f, 0f } , { 0f, 0f, 0f } , { 0f, 0f, 0f } };
        extent = new float[3] { 0f, 0f, 0f };
        translation = new float[3] { 0f, 0f, 0f };
    }

    public void SetCollider(Vector3 position, UnityEngine.Quaternion quat, Vector3 scale)
    {
        Center[0] = position.x;
        Center[1] = position.y;
        Center[2] = position.z;
        
        float[,] matrix = new float[3, 3];
        QuatToMatrix(quat, ref axis);

        Extent[0] = scale.x / 2f;
        Extent[1] = scale.y / 2f;
        Extent[2] = scale.z / 2f;
    }

    void QuatToMatrix(UnityEngine.Quaternion quat, ref float[,] m)
    {

        float wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;


        // 계수 계산
        x2 = quat.x + quat.x; y2 = quat.y + quat.y;
        z2 = quat.z + quat.z;
        xx = quat.x * x2; xy = quat.x * y2; xz = quat.x * z2;
        yy = quat.y * y2; yz = quat.y * z2; zz = quat.z * z2;
        wx = quat.w * x2; wy = quat.w * y2; wz = quat.w * z2;


        m[0, 0] = 1.0f - (yy + zz);
        m[1, 0] = xy - wz;
        m[2, 0] = xz + wy;

        m[0, 1] = xy + wz;
        m[1, 1] = 1.0f - (xx + zz);
        m[2, 1] = yz - wx;


        m[0, 2] = xz - wy;
        m[1, 2] = yz + wx;
        m[2, 2] = 1.0f - (xx + yy);
    }
    
}
