﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject boxSample;

    List<RigidBody> boxList = new List<RigidBody>();
    int numBox = 30;

    // Start is called before the first frame update
    void Start()
    {
        //numBox 개수만큼 RigidBody 생성 후 List에 Add
        for (int i = 0; i < numBox; ++i)
        {
            RigidBody c = new RigidBody();
            InitializeRigidBody(ref c);
            boxList.Add(c);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // RigidBody들 Update
        for (int i = 0; i < boxList.Count; ++i)
        {
            boxList[i].UpdatePosition();
        }

        // Collision Check 및 Speed Change
        CollisionCheckAndSpeedChange();
    }

    void SpeedChange(int i, int j)
    {
        Vector3 tmp1 = boxList[i].Speed;
        boxList[i].Speed = boxList[j].Speed;
        boxList[j].Speed = tmp1;
    }

    // 모든 RigidBody 순회하면서 Collision 되는지 확인한 뒤 Collision이 일어났다면 해당하는 RigidBody의 Speed 변경
    void CollisionCheckAndSpeedChange()
    {
        for (int i = 0; i < boxList.Count; ++i)
        {
            for (int j = 0; j < boxList.Count; ++j)
            {
                if (i < j && CollisionDetection.OBBCollisionDetection(boxList[i].boxOBBCollider, boxList[j].boxOBBCollider))
                {
                    Debug.Log(i + " " + j + " " + "Collision!");
                    SpeedChange(i, j);
                }
            }
        }
    }

    //RigidBody 초기화
    void InitializeRigidBody(ref RigidBody rb)
    {
        GameObject cube = Resources.Load<GameObject>("Cube");
        rb.Box = Instantiate(cube);                                         // 초기 박스 생성
        //rb.Scale = new Vector2(GetRandom(0f, 2f), GetRandom(0f, 2f));
        rb.Scale = new Vector3(0.3f, 0.3f, 0.3f);                                 // Scale 고정

        // 겹치지 않게 Position 설정
        float xPos = 0f;
        float yPos = 0f;
        float zPos = 0f;
        CheckBeforeInitPosition(ref xPos, ref yPos, ref zPos);
        //Debug.Log("xPos = " + xPos + "yPos = " + yPos + "zPos = " + zPos);
        rb.Position = new Vector3(xPos, yPos, zPos);

        rb.Rotation = new UnityEngine.Quaternion(GetRandom(-1f, 1f), GetRandom(-1f, 1f), GetRandom(-1f, 1f), GetRandom(-1f, 1f));

        rb.Speed = new Vector3(GetRandom(-3f, 3f), GetRandom(-3f, 3f), GetRandom(-3f, 3f));
        rb.Color = new Color(GetRandom(0f, 1f), GetRandom(0f, 1f), GetRandom(0f, 1f));
    }

    // 이전에 생성한 RigidBody와 겹치지 않는지 판단한 뒤 Position 결정
    void CheckBeforeInitPosition(ref float xPos, ref float yPos, ref float zPos)
    {
        while (true)
        {
            xPos = GetRandom(-4f, 4f);
            yPos = GetRandom(-4f, 4f);
            zPos = GetRandom(-4f, 4f);
            int count = 0;
            for (int i = 0; i < boxList.Count; ++i)
            {
                Vector3 min = new Vector3(boxList[i].Position.x - boxList[i].Scale.x, boxList[i].Position.y - boxList[i].Scale.y, boxList[i].Position.z - boxList[i].Scale.z);
                Vector3 max = new Vector3(boxList[i].Position.x + boxList[i].Scale.x, boxList[i].Position.y + boxList[i].Scale.y, boxList[i].Position.z + boxList[i].Scale.z);
                if (xPos >= min.x && xPos <= max.x && yPos >= min.y && yPos <= max.y && zPos >= min.z && zPos <= max.z)
                    count++;
            }
            if (count == 0)
                break;
        }
    }

    // min보다 크도 max보다 작은 랜덤 변수 생성
    float GetRandom(float min, float max)
    {
        return UnityEngine.Random.Range(min, max);
    }
    int GetRandom(int min, int max)
    {
        return UnityEngine.Random.Range(min, max);
    }
}
